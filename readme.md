# ENSAPAY Deployment

This repo is for deploying and running Ensapay microservices using Docker Compose script.

## Requirments
Please install these requirements before setting up the porject on your local machine :

| Requirement | DESCRIPTION                                                                         |
| ----------- | ----------------------------------------------------------------------------------- |
| JDK 8       | At least JDK 8 is required to compile the services                                  |
| Maven       | Java application packaging system   |
| Docker    | Containerization system |


## Deployment
1. First you'll need to build docker images for all services, follow docker build instructions in `readme`.
2. Deployment script of CMI services is found in `docker-compose.yml` file To execute it, run this command:
```sh
docker-compose up
```
3. Deployment script of ENSAPAY services is found in `docker-compose.ensapay.yml` file To execute it, run this command:
```sh
docker-compose -f docker-compose.ensapay.yml up
```
## Testing
Exposed ports:

| Port | Service                                                                         |
| ----------- | ----------------------------------------------------------------------------------- |
| 8000       | CMI Server                                  |
| 8001       | ENSAPAY Server   |
| 8500    | CMI Consul Dashboard |
| 8501    | ENSAPAY Consul Dashboard |

## Author

LAHMIDI Oussama

